This project is meant to be triggered by Airbyte with the following configuration:

![airbyte_config.png](./docs/airbyte_config.png)

It reads tables from Airbyte's destination schema and creates new tables that are more accessible by Superset.


### Resources to learn DBT:
- Learn more about dbt [in the docs](https://docs.getdbt.com/docs/introduction)
- Check out [Discourse](https://discourse.getdbt.com/) for commonly asked questions and answers
- Join the [chat](https://community.getdbt.com/) on Slack for live discussions and support
- Find [dbt events](https://events.getdbt.com) near you
- Check out [the blog](https://blog.getdbt.com/) for the latest news on dbt's development and best practices
