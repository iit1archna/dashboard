SELECT (t1.logtime::DATE-CURRENT_DATE) AS "Order Duration",
          t1.exchange AS "Exchange",
          t1.order_id AS "Order Id",
          t2.ticket AS "Ticket",
          t1.name AS "Task Name",
          t1.createdon,
		  CASE 
        WHEN t1.owner IN ('metromigration.orderE2E', 'metromigrationV2.orderE2E', 'metromigrationV3.orderE2E','unknown') or t1.status = 'RELEASED' THEN 'Nobody'
        WHEN t1.owner IN ('rhpamAdmin') AND t1.st='Reserved' THEN t1.actualowner_id 
        ELSE t1.owner
    END AS "Assigned To",
    CONCAT(
        CASE 
            WHEN t1.owner IN ('rhpamAdmin') AND t1.st='Reserved' THEN t1.actualowner_id 
            WHEN t1.status = 'RELEASED' THEN ''
            ELSE t3.user
        END,
        '-',
        CASE 
            WHEN t1.owner IN ('rhpamAdmin') THEN 'NSOS Broadband Services'
            WHEN t1.status = 'RELEASED' THEN ''
            ELSE t3.displayname
        END
    ) AS "User & Group",
          t4.a_counts AS "Attachments",
          t5.comment_counts AS "Comments",
          t1.logtime::DATE AS "Start Date"
   FROM
     (SELECT order_id,
             logtime,
             formname,
             st,
             actualowner_id,
             owner,
             status,
             name,
             createdon,
             rnk,
             exchange,
             max(rnk) OVER (PARTITION BY order_id,
                                         formname,
                                         status) AS max
      FROM
        (SELECT a.processinstanceid AS order_id,
                description,
                formName,
                actualowner_id,
                a.status AS st,
                CAST(logtime AS TIMESTAMP) AT TIME ZONE 'utc' AT TIME ZONE 'bst' AS logtime,
                                                                                    CAST(createdon AS TIMESTAMP) AT TIME ZONE 'utc' AT TIME ZONE 'bst' AS createdon,
                                                                                                                                                          userid AS owner,
                                                                                                                                                          a.name AS name,
                                                                                                                                                          type AS status,
                                                                                                                                                                  split_part(split_part(description, '|', 3), '|', -1) AS exchange,
                                                                                                                                                                  RANK() OVER (PARTITION BY a.processinstanceid,
                                                                                                                                                                                            formname,
                                                                                                                                                                                            type
                                                                                                                                                                               ORDER BY a.processinstanceid,
                                                                                                                                                                                        logtime) AS rnk
         FROM raw.Task a
         INNER JOIN raw.TaskEvent b ON a.id = b.taskId
         INNER JOIN raw.processinstancelog p ON a.processId = p.processId
         AND a.processinstanceid = p.processinstanceid
         WHERE p.status IN (0,
                            1)
           AND a.processId IN ('metromigration.orderE2E',
                               'metromigrationV2.orderE2E','metromigrationV3.orderE2E')
           AND a.processinstanceid NOT IN (11242,
                                           11232)
           AND (b.taskid,b.processInstanceId,b.logtime) IN
             (SELECT taskid, processInstanceId, MAX(logtime )
              FROM raw.taskevent
              GROUP BY taskid, processInstanceId)
         ORDER BY logtime DESC) AS a
      WHERE status IN ('ADDED',
                       'STARTED','DELEGATED','RELEASED')
      GROUP BY order_id,
               logtime,
               formname,
               st,
             actualowner_id,
               owner,
               status,
               name,
               createdon,
               rnk,
               exchange) AS t1
   LEFT JOIN 
   (SELECT
    ticket,
    log_date,
    value,
    processinstanceid
FROM (
    SELECT
        CASE
            WHEN value LIKE '%RetrieveTicketResponseDto%' THEN TRIM(SUBSTRING(value, STRPOS(value, 'CHG'), 10))
            ELSE TRIM(SUBSTRING(value, STRPOS(value, 'ticketNumber": "CHG') + 16, 10))
        END AS ticket,
        v.log_date,
        value,
        n.processinstanceid,
        ROW_NUMBER() OVER (PARTITION BY n.processinstanceid ORDER BY v.log_date DESC) AS row_num
    FROM
        raw.variableinstancelog v
        INNER JOIN raw.nodeinstancelog n ON n.referenceId = v.processInstanceId
    WHERE
        (value LIKE '%RetrieveTicketResponseDto%' OR value LIKE '%ticketNumber": "CHG%')
        AND v.log_date IN (
            SELECT MAX(v.log_date)
            FROM raw.variableinstancelog v
            INNER JOIN raw.nodeinstancelog n ON n.referenceId = v.processInstanceId
            WHERE
                (value LIKE '%RetrieveTicketResponseDto%' OR value LIKE '%ticketNumber": "CHG%')
            GROUP BY n.processInstanceId
        )
) AS subquery
WHERE
    row_num = 1
) AS t2 ON t1.order_id = t2.processinstanceid

   LEFT JOIN
     (SELECT x.user_id AS user,
             y.displayname
      FROM raw.group_members x
      INNER JOIN raw.groups y ON x.group_id = y.groupid AND y.groupid='NIT'
      AND x.creationdate =
        (SELECT MAX(creationdate)
         FROM raw.group_members AS g_table
         WHERE g_table.user_id = x.user_id)
      GROUP BY x.user_id,
               y.displayname) AS t3 ON t1.owner = t3.user
   LEFT JOIN
     (SELECT b.processinstanceid,
             COUNT(a.name) AS a_counts
      FROM raw.attachment a
      INNER JOIN raw.task b ON a.taskdata_attachments_id = b.id
      GROUP BY b.processinstanceid) AS t4 ON t1.order_id = t4.processinstanceid
   LEFT JOIN
     (SELECT b.processinstanceid,
             COUNT(a.text) AS comment_counts
      FROM raw.task_comment a
      INNER JOIN raw.task b ON a.taskdata_comments_id = b.id
      GROUP BY b.processinstanceid) AS t5 ON t1.order_id = t5.processinstanceid
   WHERE t1.name = 'Create Patching Schedule'
     AND t1.createdon::DATE > CURRENT_DATE - INTERVAL '31 days'
   ORDER BY t1.logtime DESC
