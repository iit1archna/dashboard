SELECT name AS name,
       sum("Unassigned Tasks") AS "SUM(Unassigned Tasks)",createdon
FROM
  (SELECT name AS name,
          owner AS owner,
                   count(DISTINCT description) AS "Unassigned Tasks",createdon
   FROM
     (select order_id,
             logtime,
             formname,
             description,
             owner,
             status,
             name,
             createdon,
             rnk,
             exchange,
             max(rnk) over(partition by order_id, formname, status) as max
      from
        (select a.processinstanceid as order_id,
                description,
                formName,
                cast(logtime as timestamp) at time zone 'utc' at time zone 'bst' as logtime,
                                                                                    cast(createdon as timestamp) at time zone 'utc' at time zone 'bst' as createdon,
                                                                                                                                                          userid as owner,
                                                                                                                                                          a.name as name,
                                                                                                                                                          type as status,
                                                                                                                                                                  split_part(split_part(description, '|', 3), '|', -1) as exchange,
                                                                                                                                                                  rank() over(partition by a.processinstanceid, formname, type
                                                                                                                                                                              order by a.processinstanceid, logtime) as rnk
         from raw.Task a,
              raw.TaskEvent b,
              raw.processinstancelog p
         where a.id=b.taskId
           AND p.status in (0,
                            1)
           AND a.processId = p.processId
           and a.workitemid=b.workitemid
           and a.processinstanceid=b.processinstanceid
           AND a.processinstanceid=p.processinstanceid
           and a.processId in ('metromigration.orderE2E',
                               'metromigrationV2.orderE2E')
           and a.processinstanceid not in (11242,
                                           11232)
           AND b.logtime in
             (select max(logtime)
              from raw.taskevent nl
              group by processInstanceId)
         order by logtime DESC) as a
      where status in ('ADDED',
                       'STARTED')
        and name != 'Plan Metro Migration'
        AND createdon::DATE > current_date - interval '31 days'
        AND a.owner in ('metromigration.orderE2E',
                        'metromigrationV2.orderE2E') ) AS virtual_table
   GROUP BY name,
            owner,createdon
   ORDER BY "Unassigned Tasks" DESC) AS virtual_table
GROUP BY name,createdon
ORDER BY "SUM(Unassigned Tasks)" DESC
