SELECT DISTINCT ON (t1.orderid)
		   t1.exchange AS "Exchange",
		   t1.orderid AS "Order Id",
		   t1.ticket AS "Ticket",
		   t6.nodename AS "Task Name",
		   t1.createdon,
		   CASE
			   WHEN t6.nodetype = 'TimerNode' THEN 'Nobody'
			   ELSE t3.user
		   END AS "Assigned To",
		   CASE
			   WHEN t6.nodetype = 'TimerNode' THEN 'SubProcess Task(DNE)'
			   ELSE CONCAT(t3.user, '-', t3.displayname)
		   END AS "User & Group",
		   t5.comment_counts AS "Comments",
		   t4.a_counts AS "Attachments",
		   t1.implementationDate AS "Implementation Date",
		   t1.daysToImplementation AS "Days to Implement"
	FROM (
		SELECT DISTINCT ON (t.processInstanceId)
			   split_part(split_part(t.description, '|', 3), '|', -1) AS exchange,
			   t.processInstanceId AS orderid,
			   t.createdon AS createdon,
			   n.nodetype AS nodetype,
			   n.nodename AS taskname,
			   b.userid AS owner,
			   TRIM(SUBSTRING(value, STRPOS(value, 'CHG'), 10)) AS ticket,
			   TO_DATE(TRIM(SUBSTRING(value, STRPOS(value, 'endDate') + 8, 10)), 'dd/mm/YYYY') AS implementationDate,
			   (TO_DATE(TRIM(SUBSTRING(value, STRPOS(value, 'endDate') + 8, 10)), 'dd/mm/YYYY') - CURRENT_DATE) AS daysToImplementation,
			   v.value AS value
		FROM raw.processinstancelog p
		JOIN raw.nodeinstancelog n ON p.processInstanceId = n.processInstanceId
		AND n.log_date IN (
			SELECT MAX(log_date)
			FROM raw.nodeinstancelog
			GROUP BY processInstanceId
		)
		JOIN raw.variableinstancelog v ON n.referenceId = v.processInstanceId
		AND v.value LIKE '%RetrieveTicketResponseDto%'
		AND v.log_date IN (
			SELECT MAX(log_date)
			FROM raw.variableinstancelog
			GROUP BY processInstanceId
		)
		JOIN raw.Task t ON t.processInstanceId = n.processInstanceId
		AND t.processId = p.processId
		AND p.processId IN ('metromigration.orderE2E', 'metromigrationV2.orderE2E')
		JOIN raw.TaskEvent b ON t.id = b.taskId
		AND b.logtime IN (
			SELECT MAX(logtime)
			FROM raw.TaskEvent
			GROUP BY processInstanceId
		)
	) AS t1
	LEFT JOIN (
		SELECT x.user_id AS user,
			   y.displayname
		FROM raw.group_members x
		INNER JOIN raw.groups y ON x.group_id = y.groupid
		AND x.creationdate = (
			SELECT MAX(creationdate)
			FROM raw.group_members AS g_table
			WHERE g_table.user_id = x.user_id
		)
	) AS t3 ON t1.owner = t3.user
	LEFT JOIN (
		SELECT b.processinstanceid,
			   COUNT(a.name) AS a_counts
		FROM raw.attachment a
		INNER JOIN raw.task b ON a.taskdata_attachments_id = b.id
		GROUP BY b.processinstanceid
	) AS t4 ON t1.orderid = t4.processinstanceid
	LEFT JOIN (
		SELECT b.processinstanceid,
			   COUNT(a.text) AS comment_counts
		FROM raw.task_comment a
		INNER JOIN raw.task b ON a.taskdata_comments_id = b.id
		GROUP BY b.processinstanceid
	) AS t5 ON t1.orderid = t5.processinstanceid
	LEFT JOIN (
		SELECT n.nodename,
			   n.nodetype,
			   p.parentprocessinstanceid
		FROM raw.nodeinstancelog n,
			 raw.processinstancelog p
		WHERE n.processInstanceId = p.processInstanceId
		AND nodetype = 'TimerNode'
		AND (n.processinstanceid, n.log_date) IN (
			SELECT processinstanceid,
				   MAX(log_date)
			FROM raw.nodeinstancelog
			GROUP BY processinstanceid
		)
		GROUP BY n.nodename,
				 n.nodetype,
				 p.parentprocessinstanceid
	) AS t6 ON t1.orderid = t6.parentprocessinstanceid
	WHERE t1.createdon::DATE > current_date - INTERVAL '31 days'
	AND t1.daysToImplementation > 0
	ORDER BY t1.orderid, t1.createdon DESC
