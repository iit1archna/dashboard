SELECT (t1.logtime::DATE-CURRENT_DATE) AS "Order Duration",
          t1.exchange AS "Exchange",
          t1.order_id AS "Order Id",
          t2.ticket AS "Ticket",
          t1.name AS "Task Name",
          t1.createdon,
		  CASE 
          WHEN t1.owner IN ('metromigration.orderE2E', 'metromigrationV2.orderE2E', 'metromigrationV3.orderE2E','unknown') or t1.status = 'RELEASED'
          THEN 'Nobody'
          ELSE t1.owner
      END AS "Assigned To",
          t8.userGroup as "Assigned Group",
          t4.a_counts AS "Attachments",
          t4.comment_counts AS "Comments",
          t1.logtime::DATE AS "Start Date"
   FROM
     (SELECT order_id,
             logtime,
             formname,
             owner,
             status,
             name,
             createdon,
             rnk,
             exchange,
             max(rnk) OVER (PARTITION BY order_id,
                                         formname,
                                         status) AS max
      FROM
        (SELECT a.processinstanceid AS order_id,
                description,
                formName,
                CAST(logtime AS TIMESTAMP) AT TIME ZONE 'utc' AT TIME ZONE 'bst' AS logtime,
                                                                                    CAST(createdon AS TIMESTAMP) AT TIME ZONE 'utc' AT TIME ZONE 'bst' AS createdon,
                                                                                                                                                          userid AS owner,
                                                                                                                                                          a.name AS name,
                                                                                                                                                          type AS status,
                                                                                                                                                                  split_part(split_part(description, '|', 3), '|', -1) AS exchange,
                                                                                                                                                                  RANK() OVER (PARTITION BY a.processinstanceid,
                                                                                                                                                                                            formname,
                                                                                                                                                                                            type
                                                                                                                                                                               ORDER BY a.processinstanceid,
                                                                                                                                                                                        logtime) AS rnk
         FROM raw.Task a
         INNER JOIN raw.TaskEvent b ON a.id = b.taskId
         INNER JOIN raw.processinstancelog p ON a.processId = p.processId
         AND a.processinstanceid = p.processinstanceid
         WHERE p.status IN (0,
                            1)
           AND a.processId IN ('metromigration.orderE2E',
                               'metromigrationV2.orderE2E','metromigrationV3.orderE2E')
           AND a.processinstanceid NOT IN (11242,
                                           11232)
           AND a.status != 'Completed'
           AND (b.taskid,b.processInstanceId,b.logtime) IN
             (SELECT taskid, processInstanceId, MAX(logtime )
              FROM raw.taskevent
              GROUP BY taskid, processInstanceId)
         ORDER BY logtime DESC) AS a
      WHERE status IN ('ADDED',
                       'STARTED','DELEGATED','RELEASED')
      GROUP BY order_id,
               logtime,
               formname,
               owner,
               status,
               name,
               createdon,
               rnk,
               exchange) AS t1
   LEFT JOIN 
   (SELECT
    ticket,
    log_date,
    value,
    processinstanceid
FROM (
    SELECT
        CASE
            WHEN value LIKE '%RetrieveTicketResponseDto%' THEN TRIM(SUBSTRING(value, STRPOS(value, 'CHG'), 10))
            ELSE TRIM(SUBSTRING(value, STRPOS(value, 'ticketNumber": "CHG') + 16, 10))
        END AS ticket,
        v.log_date,
        value,
        n.processinstanceid,
        ROW_NUMBER() OVER (PARTITION BY n.processinstanceid ORDER BY v.log_date DESC) AS row_num
    FROM
        raw.variableinstancelog v
        INNER JOIN raw.nodeinstancelog n ON n.referenceId = v.processInstanceId
    WHERE
        (value LIKE '%RetrieveTicketResponseDto%' OR value LIKE '%ticketNumber": "CHG%')
        AND v.log_date IN (
            SELECT MAX(v.log_date)
            FROM raw.variableinstancelog v
            INNER JOIN raw.nodeinstancelog n ON n.referenceId = v.processInstanceId
            WHERE
                (value LIKE '%RetrieveTicketResponseDto%' OR value LIKE '%ticketNumber": "CHG%')
            GROUP BY n.processInstanceId
        )
) AS subquery
WHERE
    row_num = 1
) AS t2 ON t1.order_id = t2.processinstanceid
   LEFT JOIN
     (SELECT b.processinstanceid,
             COUNT(DISTINCT a.name) AS a_counts,
             COUNT(DISTINCT c.text) AS comment_counts
      FROM raw.task b
      LEFT JOIN raw.attachment a ON a.taskdata_attachments_id = b.id
      LEFT JOIN raw.task_comment c ON c.taskdata_comments_id = b.id
      GROUP BY b.processinstanceid) AS t4 ON t1.order_id = t4.processinstanceid

   LEFT JOIN (
    SELECT grp.displayname AS userGroup, x.processinstanceid AS orderid
    FROM raw.task x
    INNER JOIN raw.peopleassignments_potOwners owners ON owners.task_id = x.id  
    INNER JOIN raw.groups grp ON grp.groupid = owners.entity_id
    INNER JOIN (
        SELECT processinstanceid, MAX(createdon) AS max_createdon
        FROM raw.task
        GROUP BY processinstanceid
    ) AS max_created ON x.processinstanceid = max_created.processinstanceid AND x.createdon = max_created.max_createdon
    GROUP BY userGroup, orderid
) AS t8 ON t1.order_id = t8.orderid

   WHERE (t1.owner IN ('metromigration.orderE2E', 'metromigrationV2.orderE2E', 'metromigrationV3.orderE2E','unknown') or t1.status = 'RELEASED')
     AND t1.name != 'Plan Metro Migration'
     AND t1.createdon::DATE > CURRENT_DATE - INTERVAL '31 days'
   ORDER BY t1.logtime DESC
