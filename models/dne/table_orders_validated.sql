SELECT t1.exchange AS "Exchange",
          t1.order_id AS "Order Id",
          t1.status AS "Task Name",
          t1.userid AS "Completed By",
          t1.logtime as "Completed on"
   FROM
     (SELECT distinct split_part(split_part(b.description, '|', 3), '|', -1) AS exchange,
                      a.processinstanceid as order_id,
                      b.name as status,
                      a.userid as userid,
                      a.logtime as logtime,
                      a.taskid
      from raw.taskevent a,
           raw.task b
      WHERE type in ('COMPLETED')
        and logtime in
          (select max(logtime)
           from raw.taskevent
           group by processinstanceid)
        and a.processinstanceid=b.processinstanceid
        and b.name='Validate Migration Order'
        and b.id=a.taskid) AS t1
   WHERE t1.logtime::DATE > current_date - interval '31 days'
     and EXTRACT(YEAR
                 FROM current_date)=EXTRACT(YEAR
                                            FROM t1.logtime::DATE)
     and EXTRACT(MONTH
                 FROM current_date)=EXTRACT(MONTH
                                            FROM t1.logtime::DATE)
   ORDER by t1.logtime DESC
