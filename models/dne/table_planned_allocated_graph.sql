SELECT name AS name,
       owner AS owner,
       sum("Planned_allocated Tasks") AS "SUM(Planned_allocated Tasks)",createdon
FROM
  (SELECT name AS name,
          owner AS owner,
                   count(DISTINCT description) AS "Planned_allocated Tasks",createdon
   FROM
     (SELECT order_id,
             logtime,
             formname,
             description,
             owner,
             status,
             name,
             createdon,
             rnk,
             max(rnk) OVER (PARTITION BY order_id,
                                         formname,
                                         status) AS max
      FROM
        (SELECT a.processinstanceid AS order_id,
                description,
                formName,
                CAST(logtime AS TIMESTAMP) AT TIME ZONE 'utc' AT TIME ZONE 'bst' AS logtime,
                                                                                    CAST(createdon AS TIMESTAMP) AT TIME ZONE 'utc' AT TIME ZONE 'bst' AS createdon,
                                                                                                                                                          userid AS owner,
                                                                                                                                                          a.name AS name,
                                                                                                                                                          type AS status,
                                                                                                                                                                  split_part(split_part(description, '|', 3), '|', -1) AS exchange,
                                                                                                                                                                  RANK() OVER (PARTITION BY a.processinstanceid,
                                                                                                                                                                                            formname,
                                                                                                                                                                                            type
                                                                                                                                                                               ORDER BY a.processinstanceid,
                                                                                                                                                                                        logtime) AS rnk
         FROM raw.Task a
         INNER JOIN raw.TaskEvent b ON a.id = b.taskId
         INNER JOIN raw.processinstancelog p ON a.processId = p.processId
         AND a.processinstanceid = p.processinstanceid
         WHERE p.status IN (0,
                            1)
           AND a.processId IN ('metromigration.orderE2E',
                               'metromigrationV2.orderE2E')
           AND a.processinstanceid NOT IN (11242,
                                           11232)
           AND b.logtime IN
             (SELECT MAX(logtime)
              FROM raw.taskevent nl
              GROUP BY processInstanceId)
         ORDER BY logtime DESC) AS a
      WHERE status IN ('ADDED',
                       'STARTED')
        and name != 'Plan Metro Migration'
        AND createdon::DATE > current_date - interval '31 days'
        AND a.owner NOT in ('metromigration.orderE2E',
                            'metromigrationV2.orderE2E') ) AS virtual_table
   GROUP BY name,
            owner,createdon
   ORDER BY "Planned_allocated Tasks" DESC) AS virtual_table
GROUP BY name,owner,createdon
ORDER BY "SUM(Planned_allocated Tasks)" DESC
